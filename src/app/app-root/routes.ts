import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

export const routes = [
    {
        path: '',
        redirectTo: 'albums',
        pathMatch: 'full'
    },
    {
        path: 'albums',
        loadChildren: '../albums/album.module#AlbumModule'
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
