import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

import { AlbumRestService } from '../services/album.rest.service';
import { PhotoDetailModal } from '../photo-detail-modal/photo-detail-modal';

@Component({
    selector: "photo-list",
    templateUrl: "./photo-list.view.html",
    styleUrls:['./photo-list.style.scss']
})

export class PhotoListComponent {
    rows: any;
    totalCount: number = 5000;
    loadingIndicator: boolean = true;
    showTable: boolean = false;
    start: number = 0;
    limit: number = 20;
    albumId: number;
    ownerName: string;
    albumTitle: string;

    constructor(public albumRestService: AlbumRestService, private route: ActivatedRoute,
        public dialog: MatDialog) {
        this.albumTitle = this.route.queryParams["value"]["albumTitle"];
        this.ownerName = this.route.queryParams["value"]["username"];
        this.route.params.subscribe( params => {
            this.albumId = params['id'];
        });
        this.getPhotos();
    }

    columns = [
        { prop: 'title', name: 'Title' },
        { prop: 'thumbnailUrl', name: 'Thumbnail' }
    ];

    getPhotos(filterObj?) {
        let __self = this;
        if (!filterObj) {
            filterObj = {
                _start: 0,
                _limit: 20
            }
        }

        filterObj.queryParam = this.albumId;

        this.albumRestService.getPhotos(filterObj).subscribe(function (photosResponse) {
            __self.rows = photosResponse;
            __self.totalCount = 5000;
            __self.showTable = true;
        });

    }

    getPagedData(pageInfo) {
        this.getPhotos({
            _start: pageInfo.pageIndex,
            _limit: pageInfo.pageSize
        });
    }

    openDialog(event) {
        if (event.type == 'click') {
            let data = event.row;
            console.log(data);
            let modalData = {
                ownerName : this.ownerName,
                albumTitle : this.albumTitle,
                photoTitle: data.title,
                photoUrl: data.url
            }
            this.dialog.open(PhotoDetailModal, {
                data: modalData
            });
        }
    }

    // getAlbumById(albumId){
    //     let __self = this;
    //     this.albumRestService.getAlbumById(albumId).subscribe(function(albumResponse){
    //         __self.albumTitle = albumResponse[0].title;
    //     });
    // }
}