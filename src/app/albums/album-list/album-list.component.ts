import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { UtilityService } from '../../shared-services/utility-service';
import { AlbumRestService } from '../services/album.rest.service';
import { AlbumService } from '../services/album.service';

@Component({
    selector: "album-list",
    templateUrl: "./album-list.view.html",
    styleUrls:['./album-list.style.scss']
})

export class AlbumListComponent implements OnInit {
    title = "Album List Info";

    rows: any;
    totalCount: number = 5000;
    loadingIndicator: boolean = true;
    showTable: boolean = false;
    start : number = 0;
    limit : number = 20;

    columns = [
        { prop: 'placeholderColor', name: 'Color' },
        { prop: 'covorImage', name: 'Cover Photo' },
        { prop: 'albumTitle', name: 'Title'},
        { prop: 'username', name: 'User Name' }
    ];

    constructor(private utService: UtilityService, public albumRestService: AlbumRestService,
        public albumService: AlbumService, public router: Router) {
        this.getAlbums();
    }

    ngOnInit() {

    }

    redirectTo(event){
        if(event.type == 'click') {
            let data = event.row;
            console.log(event.row);
            this.router.navigateByUrl('/albums/' + data.albumId + '/photos?albumTitle=' + data.albumTitle + "&username=" + data.username);
        }
    }

    getAlbums(filterObj?) {
        let __self = this;
        if (!filterObj) {
            filterObj = {
                _start: 0,
                _limit: 20
            }
        }

        this.albumRestService.getAlbums(filterObj).subscribe(function (albumsResponse) {
            let userIds = Array.from(new Set(__self.albumService.prepareUserIdList(albumsResponse)));
            let userListFilterObj = {
                _start: 0,
                _limit: userIds["length"],
                query: 'id=' + userIds.join('&id=')
            };
            __self.albumRestService.getUsersIds(userListFilterObj).subscribe(function (userListResponse) {
                let data = __self.albumService.mergedAlbumsUsers(albumsResponse, userListResponse);
                __self.rows = data;
                __self.totalCount = 5000;
                __self.showTable = true;
            });
        });
    }

    getPagedData(pageInfo) {
        this.getAlbums({ 
            _start : pageInfo.pageIndex,
            _limit : pageInfo.pageSize
        });
    }
}