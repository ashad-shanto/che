import { Injectable } from '@angular/core';
import { UtilityService } from '../../shared-services/utility-service';

@Injectable()
export class AlbumService {

    /**
     *
     */
    constructor(public utilityService : UtilityService ) {
        
    }

    /**
     * prepareAlbumList
     */
    public prepareUserIdList(albumList) {
        let userIds = [];
        albumList.forEach(element => {
            let { userId } = element;
            userIds.push(userId);
        });
        return userIds;
    }

    private mapUsers(userList){
        let mappedObj = {}, __self = this;
        userList.map(function(data){
            let itemKey = data.id;
            data.placeholderColor = __self.utilityService.getRandomColor();
            mappedObj[itemKey] = data;

        });
        return mappedObj;
    }

    /**
     * mergedAlbumsUsers
     */
    public mergedAlbumsUsers(albums, users) {
        let mergedTableData = [];
        let mappedUsersObj = this.mapUsers(users);
        albums.forEach(function(element){
            let userInfo = mappedUsersObj[element.userId]
            let mappedAlbum = {
                covorImage : 'https://via.placeholder.com/600/771796',
                placeholderColor : userInfo.placeholderColor,
                albumTitle : element.title,
                username : userInfo.username,
                albumId: element.id,
                userId: userInfo.id
            };
            mergedTableData.push(mappedAlbum);
        });
        return mergedTableData;
    }
}