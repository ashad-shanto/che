import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class AlbumRestService {

    /**
     *
     */
    constructor(private http : HttpClient) {
        
    }

    /**
     * getAlbums
    */
    public getAlbums(filterObj) {
        return this.http.get(environment.jsonPlaceHolder + 'albums?_start=' + filterObj._start + '&_limit=' + filterObj._limit);
    }

    public getAlbumsCount() {
        return this.http.get(environment.jsonPlaceHolder + 'albums');
    }

    public getUsersIds(filterObj){
        return this.http.get(environment.jsonPlaceHolder + 'users?' + filterObj.query  + '&_start=' + filterObj._start + '&_limit=' + filterObj._limit);
    }

    public getPhotos(filterObj) {
        return this.http.get(environment.jsonPlaceHolder + 'photos?albumId=' + filterObj.queryParam + '&_start=' + filterObj._start + '&_limit=' + filterObj._limit);
    }

    public getAlbumById(id) {
        return this.http.get(environment.jsonPlaceHolder + 'albums?id=' + id);
    }

}