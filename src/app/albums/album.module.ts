// angular modules
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatPaginatorModule } from '@angular/material/paginator';

// business apps
import { AlbumListComponent } from './album-list/album-list.component';
import { AlbumRestService } from './services/album.rest.service';
import { AlbumService } from './services/album.service';
import { PhotoListComponent } from '../albums/photo-list/photo-list.component';
import { PhotoDetailModal } from '../albums/photo-detail-modal/photo-detail-modal';

import { UtilityService } from '../shared-services/utility-service';

@NgModule({
    imports: [
        HttpClientModule,
        NgxDatatableModule,
        CommonModule,
        MatDialogModule,
        MatPaginatorModule,
        RouterModule.forChild([
            {
                path: '',
                component: AlbumListComponent
            },
            {
                path: ':id/photos',
                component: PhotoListComponent
            }
        ])
    ],
    exports: [MatDialogModule],
    declarations: [AlbumListComponent, PhotoListComponent, PhotoDetailModal],
    providers: [AlbumRestService, UtilityService, AlbumService, { provide: MatDialogRef, useValue: {} }],
    entryComponents: [PhotoDetailModal]
})

export class AlbumModule {
}