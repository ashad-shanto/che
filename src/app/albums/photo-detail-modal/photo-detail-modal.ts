import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';



export interface DialogData {
    ownerName: string,
    albumTitle: string,
    photoTitle: string,
    photoUrl: string
}

@Component({
    selector: "photo-detail-modal",
    templateUrl: "./photo-detail-modal.view.html",
    styleUrls: ['./photo-detail-modal.style.scss']
})

export class PhotoDetailModal {
    constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) { }
}